ics-ans-role-conserver-server
=============================

Ansible role to install conserver-server.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.24

Role Variables
--------------

Currently assumes that conserver client connects via UDS. Changes will be required to support telnet ports.

```yaml
conserver_server_host: Conserver server hostname
conserver_server_consoles:
  - master: IOC host - must be 'localhost' if type = 'uds'
    name: IOC name for conserver usage
	type: console type (typically 'uds')
	uds: path to UNIX domain socket

```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-conserver-server
```

License
-------

BSD 2-clause
