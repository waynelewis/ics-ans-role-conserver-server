import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_conserver_paths(host):
    assert host.file("/var/log/conserver").exists
    assert host.file("/var/log/conserver").is_directory
    assert host.file("/etc/conserver").exists
    assert host.file("/etc/conserver").is_directory
    assert host.file("/etc/conserver/procs.cf").exists
    assert host.file("/etc/conserver/procs.cf").is_file

    if host.system_info.distribution == "debian":
        conserver_config_path = "/etc/conserver"
    else:
        conserver_config_path = "/etc"

    assert host.file(os.path.join(conserver_config_path, "conserver.cf")).exists
    assert host.file(os.path.join(conserver_config_path, "conserver.cf")).is_file
    assert host.file(os.path.join(conserver_config_path, "conserver.passwd")).exists
    assert host.file(os.path.join(conserver_config_path, "conserver.passwd")).is_file


def test_conserver_server_enabled_and_running(host):
    service_name = "conserver"
    service = host.service(service_name)
    assert service.is_enabled
    assert service.is_running
